/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ufps.vistas;

import java.time.LocalTime;
import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author docente
 */
public class TestColaP {
    
    public static void main(String[] args) {
        ColaP<String> cp=new ColaP();
        cp.enColar("madarme",10);
        cp.enColar("alessandro",12);
        cp.enColar("clara",8);
        cp.enColar("felipe",1);
        cp.enColar("omar",5);
        while(!cp.esVacia())
        {
            System.out.println(cp.deColar());
        }
        
        LocalTime x=LocalTime.of(1, 1, 0);
        System.out.println(x.toSecondOfDay());
    }
    
}
