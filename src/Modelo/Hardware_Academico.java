/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Hardware_Academico {

    //tipo=true-> computador, tipo=false-> tablet
    private boolean tipo;
    private int id_hardware;
    private String descripcion;

    public Hardware_Academico() {
    }

    public Hardware_Academico(boolean tipo, int id_hardware, String descripcion) {
        this.tipo = tipo;
        this.id_hardware = id_hardware;
        this.descripcion = descripcion;
    }

    public boolean isTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public int getId_hardware() {
        return id_hardware;
    }

    public void setId_hardware(int id_hardware) {
        this.id_hardware = id_hardware;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Hardware_Academico{" + "tipo=" + tipo + ", id_hardware=" + id_hardware + ", descripcion=" + descripcion + '}';
    }
    
    
    
}
